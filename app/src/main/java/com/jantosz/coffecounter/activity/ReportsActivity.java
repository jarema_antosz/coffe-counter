package com.jantosz.coffecounter.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.jantosz.coffecounter.R;

public class ReportsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reports);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        return super.onOptionsItemSelected(item);
    }

    public void loadAllCoffesListView(View view){
        Intent intent = new Intent(this, AllCoffeesListViewActivity.class);
        startActivity(intent);
    }

    public void handleReport2Button(View view){
        Toast toast = Toast.makeText(this, "To be implemented...", Toast.LENGTH_SHORT);
        toast.show();
    }
}
