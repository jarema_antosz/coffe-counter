package com.jantosz.coffecounter.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.jantosz.coffecounter.R;
import com.jantosz.coffecounter.data.CoffeeDataSource;
import com.jantosz.coffecounter.data.MySQLiteHelper;

import java.text.DateFormat;

public class AllCoffeesListViewActivity extends ListActivity {

    private CoffeeDataSource coffeeDataSource;
    private Cursor cursor;
    private SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        coffeeDataSource = new CoffeeDataSource(this);
        coffeeDataSource.open();

        createListViewAdapter();
    }

    private void createListViewAdapter() {
        String[] columns = new String[]{
                MySQLiteHelper.COLUMN_ID,
                MySQLiteHelper.COLUMN_AMOUNT,
                MySQLiteHelper.COLUMN_CREATION_DATE
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                R.id.column_id,
                R.id.column_amount,
                R.id.column_creation_date
        };

        cursor = coffeeDataSource.fetchAllCoffees();

        cursorAdapter = new SimpleCursorAdapter(
                this, R.layout.activity_all_coffes_list_view,
                cursor,
                columns,
                to,
                0);

        cursorAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

            public boolean setViewValue(View aView, Cursor aCursor, int aColumnIndex) {

                if (aColumnIndex == 2) {
                    Long createDate = aCursor.getLong(aColumnIndex);
                    TextView textView = (TextView) aView;
                    DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getApplicationContext());
                    textView.setText(dateFormat.format(createDate));
                    return true;
                }

                return false;
            }
        });

        setListAdapter(cursorAdapter);
        this.getListView().setFastScrollEnabled(true);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_delete_coffee)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        long columnIdValue = cursor.getLong(cursor.getColumnIndex(MySQLiteHelper.COLUMN_ID));
                        coffeeDataSource.deleteCoffee(columnIdValue);
                        cursor = coffeeDataSource.fetchAllCoffees();
                        cursorAdapter.swapCursor(cursor);
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });

        builder.show();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_all_coffes_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        coffeeDataSource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        coffeeDataSource.close();
        super.onPause();
    }
}
