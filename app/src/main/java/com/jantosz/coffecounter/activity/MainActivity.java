package com.jantosz.coffecounter.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.jantosz.coffecounter.data.CoffeeDataSource;
import com.jantosz.coffecounter.R;

import java.util.Date;


public class MainActivity extends Activity implements View.OnClickListener {


    private CoffeeDataSource coffeeDataSource;
    private Button addCoffeButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        addCoffeButton = (Button) findViewById(R.id.addCoffeButton);
        addCoffeButton.setOnClickListener(this);

        coffeeDataSource = new CoffeeDataSource(this);
        coffeeDataSource.open();
        loadCoffees();

    }

    private void loadCoffees() {
        int coffes = coffeeDataSource.countAllCoffees();
        TextView textViewCoffeCounter = (TextView) findViewById(R.id.coffeCounterNumberTextView);

        textViewCoffeCounter.setText(String.valueOf(coffes));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.main_menu_send_sms_item) {
            return sendSms();
        }
        if (id == R.id.main_menu_send_email_item) {
            return sendMail();
        }
        if (id == R.id.main_menu_about_item) {
            return showAboutPopup();
        }
        if (id == R.id.main_menu_reports_item) {
            Intent intent = new Intent(this, ReportsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean showAboutPopup() {

        PackageInfo pInfo;
        String version = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String message = getResources().getString(R.string.author) + "\n" + getResources().getString(R.string.version) + " " + version;


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.main_menu_about);
        builder.setMessage(message);
        builder.show();

        return true;
    }

    private boolean sendSms() {
        String mess = getResources().getString(R.string.sms_message_text);

        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", mess);
        sendIntent.setType("vnd.android-dir/mms-sms");
        startActivity(sendIntent);

        return true;
    }

    private boolean sendMail() {
        String mess = getResources().getString(R.string.mail_message_text);

        Intent testIntent = new Intent(Intent.ACTION_VIEW);
        Uri data = Uri.parse("mailto:?subject=" + mess + "&body=" + mess);
        testIntent.setData(data);
        startActivity(testIntent);

        return true;
    }

    @Override
    public void onClick(View v) {

        if (v == addCoffeButton) {
            createAddCoffeDialog();
        }

    }

    private void createAddCoffeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();

        final View inflate = inflater.inflate(R.layout.add_coffe_popup, null);
        builder.setView(inflate)
                // Add action buttons
                .setPositiveButton(R.string.button_add_coffe, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        NumberPicker np = (NumberPicker) inflate.findViewById(R.id.coffeNumberPicker);
                        np.clearFocus();
                        int value = np.getValue();
                        storeCoffe(value);

                    }


                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.show();
        NumberPicker np = (NumberPicker) dialog.findViewById(R.id.coffeNumberPicker);
        np.setMaxValue(5);
        np.setMinValue(1);
    }

    private void storeCoffe(int value) {
        coffeeDataSource.createCoffee(value, new Date());
        Toast coffeSavedToast = Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT);
        coffeSavedToast.show();

        loadCoffees();
    }

    @Override
    protected void onResume() {
        coffeeDataSource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        coffeeDataSource.close();
        super.onPause();
    }
}
