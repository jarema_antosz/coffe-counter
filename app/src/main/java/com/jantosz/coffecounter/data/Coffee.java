package com.jantosz.coffecounter.data;

import java.util.Date;

/**
 * Created by Jarek on 15.01.15.
 */
public class Coffee {

    private long id;
    private long amount;
    private Date creationDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
