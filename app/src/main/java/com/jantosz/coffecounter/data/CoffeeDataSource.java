package com.jantosz.coffecounter.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CoffeeDataSource {

    // Database fields
    private SQLiteDatabase database;
    private MySQLiteHelper dbHelper;
    private String[] allColumns = {MySQLiteHelper.COLUMN_ID,
            MySQLiteHelper.COLUMN_AMOUNT, MySQLiteHelper.COLUMN_CREATION_DATE};

    public CoffeeDataSource(Context context) {
        dbHelper = new MySQLiteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Coffee createCoffee(long amount, Date creationDate) {
        ContentValues values = new ContentValues();
        values.put(MySQLiteHelper.COLUMN_AMOUNT, amount);
        values.put(MySQLiteHelper.COLUMN_CREATION_DATE, creationDate.getTime());
        long insertId = database.insert(MySQLiteHelper.TABLE_COFFES, null,
                values);
        Cursor cursor = database.query(MySQLiteHelper.TABLE_COFFES,
                allColumns, MySQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Coffee newComment = cursorToCoffee(cursor);
        cursor.close();
        return newComment;
    }

    public void deleteCoffee(Long id) {
        database.delete(MySQLiteHelper.TABLE_COFFES, MySQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public Cursor fetchAllCoffees() {

        Cursor mCursor = database.query(MySQLiteHelper.TABLE_COFFES,
                allColumns,
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;
    }

    public int countAllCoffees() {
        Cursor mCount = database.rawQuery("select sum(" + MySQLiteHelper.COLUMN_AMOUNT + ") from " + MySQLiteHelper.TABLE_COFFES, null);
        mCount.moveToFirst();
        int count = mCount.getInt(0);
        mCount.close();

        return count;
    }

    private Coffee cursorToCoffee(Cursor cursor) {
        Coffee comment = new Coffee();
        comment.setId(cursor.getLong(0));
        comment.setAmount(cursor.getLong(1));
        comment.setCreationDate(new Date(cursor.getLong(2)));
        return comment;
    }
} 

